package ch.teko.oop.graziano.greeter

import kotlinx.serialization.Serializable

@Serializable
data class Person(val name: String) {
    fun greet(): String {
        return "Hello, $name"
    }

    override fun toString(): String {
        return name
    }
}