package ch.teko.oop.graziano.greeter

const val jsonPath = "/Users/gfelline/tmp/json-repo/person.json"

fun main(args: Array<String>) {
    // Yuhuuuu
    val cli = CLI()
    val repository = PersonJsonRepository(jsonPath)
    val personModel = Greeter(cli, repository)
    val controller = Controller(personModel)
    cli.setController(controller)
    cli.setModel(personModel)
    cli.start()
    // ende
}