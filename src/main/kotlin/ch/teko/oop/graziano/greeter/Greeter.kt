package ch.teko.oop.graziano.greeter

class Greeter(
    val greeterView: CLI,
    val personRepository: PersonRepository
) {

    fun greet() {
        val greetings = arrayListOf<String>()
        val persons = personRepository.getAllPersons()
        persons.forEach { person ->
            greetings.add(person.greet())
        }
        greeterView.showGreet(greetings)
    }

    fun addPerson(personName: String) {
        personRepository.createPerson(personName)
        greeterView.showMenu(personRepository.getAllPersons())
    }

    fun removePerson(personName: String) {
        personRepository.removePerson(personName)
        greeterView.showMenu(personRepository.getAllPersons())
    }

    fun getAllPerson() = personRepository.getAllPersons()
}
