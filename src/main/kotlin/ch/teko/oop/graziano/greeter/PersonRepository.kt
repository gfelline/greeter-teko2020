package ch.teko.oop.graziano.greeter

interface PersonRepository {
    fun createPerson(personName: String): Person
    fun removePerson(personName: String)
    fun getAllPersons(): List<Person>
}