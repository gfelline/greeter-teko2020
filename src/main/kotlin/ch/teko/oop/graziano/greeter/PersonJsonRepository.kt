package ch.teko.oop.graziano.greeter

import kotlinx.serialization.json.Json
import kotlinx.serialization.json.JsonConfiguration
import kotlinx.serialization.list
import java.io.File

class PersonJsonRepository(val jsonPath:String) : PersonRepository {

    var persons:MutableList<Person>
    val json = Json(JsonConfiguration(prettyPrint = true))

    init {
        val file = File(jsonPath)
        if (file.exists()) {
            persons = json.parse(Person.serializer().list, file.readText()).toMutableList()
        } else {
            persons = mutableListOf()
            file.createNewFile()
            file.writeText(json.stringify(Person.serializer().list, persons))
        }
    }

    override fun createPerson(personName: String): Person {
        val file = File(jsonPath)
        val createdPerson = Person(personName)
        persons.add(createdPerson)
        file.writeText(json.stringify(Person.serializer().list, persons))
        return createdPerson
    }

    override fun removePerson(personName: String) {
        val file = File(jsonPath)
        val personToRemove = Person(personName)
        persons.remove(personToRemove)
        file.writeText(json.stringify(Person.serializer().list, persons))
    }

    override fun getAllPersons(): List<Person> {
        return persons.toList()
    }
}