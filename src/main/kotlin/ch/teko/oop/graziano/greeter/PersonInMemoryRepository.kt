package ch.teko.oop.graziano.greeter

class PersonInMemoryRepository : PersonRepository {
    val persons = arrayListOf<Person>()
    override fun createPerson(personName: String): Person {
        val newPerson = Person(personName)
        persons.add(newPerson)
        return newPerson
    }

    override fun removePerson(personName: String) {
        val personToRemove = Person(personName)
        persons.remove(personToRemove)
    }

    override fun getAllPersons(): List<Person> {
        return persons
    }
}