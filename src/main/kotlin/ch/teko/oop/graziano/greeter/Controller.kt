package ch.teko.oop.graziano.greeter

class Controller(val greeter: Greeter) {
    fun parse(command: String): Boolean {
        if ("greet" == command) {
            greeter.greet()
            return true
        } else if (command.startsWith("add ")) {
            greeter.addPerson(parseName(command))
            return true
        } else if (command.startsWith("remove ")) {
            greeter.removePerson(parseName(command))
            return true
        }
        println("Befehl $command nicht erkannt!")
        return false
    }

    private fun parseName(command: String): String {
        return command.split(" ")[1]
    }
}