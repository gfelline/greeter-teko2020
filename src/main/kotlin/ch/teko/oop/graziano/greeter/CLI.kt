package ch.teko.oop.graziano.greeter

import java.util.*

class CLI {
    private lateinit var controller: Controller
    private lateinit var greeter: Greeter

    fun setController(controller: Controller) {
        this.controller = controller
    }

    fun setModel(greeter: Greeter) {
        this.greeter = greeter
    }

    fun start() {
        val persons = greeter.getAllPerson()
        showMenu(persons)
        askForCommand()
    }

    fun showMenu(persons: List<Person>) {
        println(
            """
                persons: $persons
                Commands:
                add [name]
                remove [name]
                greet
            """.trimIndent()
        )
    }

    fun showGreet(greetings: ArrayList<String>) {
        greetings.forEach { greeting ->
            println(greeting)
        }
    }

    private fun askForCommand() {
        val scanner = Scanner(System.`in`)
        var command: String
        do {
            command = scanner.nextLine()
        } while (controller.parse(command))
    }
}